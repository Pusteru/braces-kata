import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BraceCheckerTests {

    private BraceChecker checker = new BraceChecker();

    @Test
    public void testValid() {
        assertThat(this.checker.isValid("(sadas)"), is(true));
    }

    @Test
    public void testValid2() {
        assertThat(this.checker.isValid("(s[a]{}[{w{dfss( (e)e`) }asd}]das)"), is(true));
    }

    @Test
    public void testInValid() {
        assertThat(this.checker.isValid("([)"), is(false));
    }

    @Test
    public void testInValid2() {
        assertThat(this.checker.isValid("([][f32{df{w}s}]a]fasdf)"), is(false));
    }

}
