En esta Kata trataremos el problema del correcto uso de paréntesis.

Dada una entrada de caracteres tenemos que decir si el uso de paréntesis es correcto.

Para ello deberemos asegurarnos que cada obertura de los siguientes caracteres tiene un cierre del mismo y viceversa sin interferir en el orden jerárquico: 

"(" -> ")"
"[" -> "]"
"{" -> "}"

Para añadir un poco más de complejidad se debe realizar la práctica con todas las normas que sea posible de lógica calisthenics. Que son las siguientes:

1. Un nivel de indentación por método
2. No se puede utilizar la funcionalidad else
3. Hay que envolver todas las clases primitivas o strings con objetos propios
4. Utilizar métodos propios para las colecciones envueltas no simples interfaces de la primitiva envuelta.
5. Solo se puede poner un punto por línea
6. No se pueden abreviar ningún nombre.
7. Todas las funciones deben ser como máximo de 10 líneas y las clases de 75 líneas.
8. Las clases deben tener como máximo dos variables instanciables.
9. No Getters/Setters/Properties

Mas info en http://aimforsimplicity.com/post/objectcalisthenicsgameoflifekata/

En el repositorio podréis encontrar una función llamada checkBraces con un string de entrada. Esta será la que deberéis implementar, para comprobar si es correcto tendréis que utilizar los test checkBracesTest.

Si hay algún problema ponedlo como comentario de este repo :)